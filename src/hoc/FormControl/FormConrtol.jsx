import React from 'react'
import s from './FormControl.module.scss'


export const ElementType = ElementType => ({ input, meta, ...props }) => {
    const hasError = meta.touched && meta.error
    return <div>
        <ElementType {...input}{...props} />
        {hasError && <div className={s.error}>{meta.error}</div>}
    </div>
}

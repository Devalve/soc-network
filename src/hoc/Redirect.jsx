import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'


export const mapStateToProps = state => {
    return {
        isAuth: state.auth.isAuth
    }
}


export const withRedirectComponent = Component => {
    class authRedirect extends React.Component {
        render() {
            if (!this.props.isAuth) return <Redirect to='/login' />
            return <Component {...this.props} />
        }
    }
    return connect(mapStateToProps, {})(authRedirect)
}
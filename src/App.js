import React from 'react'
import './App.scss'
import ProfileContainer from './components/Profile/ProfileContainer'
import { DialogsContainer } from './components/Dialogs/DialogsContainer'
import { News } from './components/News/News'
import { Settings } from './components/Settings/Settings'
import { Music } from './components/Music/Music'
import { UserContainer } from './components/Users/UsersContainer'
import { NavContainer } from './components/Nav/NavContainer'
import { Route, withRouter } from 'react-router-dom'
import { HeaderCont } from './components/Header/HeaderContainer'
import { connect } from 'react-redux'
import { initializeApp } from './redux/appReducer'

import Login from './components/Login/Login'
import { compose } from 'redux'
import { Preloader } from './components/Preloader/Preloader'

export class App extends React.Component {

  componentDidMount() {
    this.props.initializeApp()
  }
  render() {
    if (!this.props.initialized) {
      return <Preloader />
    }
    return <div className='app_wrapper'>

      <HeaderCont />
      <NavContainer />
      <div className='app-wrapper-content'>

        <Route
          path='/dialogs'
          render={() => <DialogsContainer />} />

        <Route
          path='/profile/:userId?'
          render={() => <ProfileContainer />} />

        <Route
          path='/news'
          render={() => <News />} />

        <Route
          path='/music'
          render={() => <Music />} />

        <Route
          path='/settings'
          render={() => <Settings />} />

        <Route
          path='/users'
          render={() => <UserContainer />} />

        <Route
          path='/login'
          render={() => <Login />} />

      </div>
    </div>
  }
}

const mapStateToProps = state => {
  return {
    initialized: state.app.initialized
  }
}

export default compose(withRouter, connect(mapStateToProps, { initializeApp }))(App)
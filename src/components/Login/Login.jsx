import React from 'react'
import s from './Login.module.scss'
import style from '../../hoc/FormControl/FormControl.module.scss'
import { reduxForm, Field } from 'redux-form'
import { ElementType } from '../../hoc/FormControl/FormConrtol'
import { required } from '../../utils/Validators/validators'
import { connect } from 'react-redux'
import { logIn } from '../../redux/authReducer'
import { Redirect } from 'react-router-dom'


const LoginForm = props => {
    const Input = ElementType('input')

    return <div className={s.mainLogin}>
        <h1 className={s.title}>LOGIN</h1>
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field
                    validate={[required]}
                    name='email'
                    component={Input}
                    placeholder='Email'
                />
            </div>
            <div>
                <Field
                    validate={[required]}
                    name='password'
                    component={Input}
                    placeholder='Password'
                    type='password' />
            </div><br />
            <div>
                <Field
                    name='rememberMe'
                    component={Input}
                    type='checkbox' /> Remember me
            </div>
            <div>
                <button>Login</button>
            </div>
            {props.error && <div className={style.error}>{props.error}</div>}
        </form>
    </div>
}

const ReduxLoginForm = reduxForm({ form: 'LOGIN' })(LoginForm)



export const Login = props => {

    const onSubmit = formData => {
        props.logIn(formData.email, formData.password, formData.rememberMe)
    }
    if (props.isAuth) {
        return <Redirect to='/profile' />
    }
    return <ReduxLoginForm onSubmit={onSubmit} />

}

const mapStateToProps = state => ({ isAuth: state.auth.isAuth })
export default connect(mapStateToProps, { logIn })(Login)
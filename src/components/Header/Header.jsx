import React from 'react'
import s from './Header.module.scss'
import { NavLink, Redirect } from 'react-router-dom'
import { reduxForm, Field } from 'redux-form'

export let HeaderForm = props => {


    return <form onSubmit={props.handleSubmit}>
        <Field
            name='searchInput'
            component='input'
            className={s.search_input} />
    </form>


}


const HeaderReduxForm = reduxForm({ form: 'HEADER_FORM' })(HeaderForm)

export const Header = props => {

    return <>
        <header className={s.header} >
            <img src='http://file03.16sucai.com/2017/1100/16sucai_V579E6F146.JPG' alt=''></img>
            <HeaderReduxForm
            />

            <div className={s.login}>
                {props.isAuth
                    ? <em>{props.login}  <button onClick={props.logOut}>Log out</button></em>
                    :  <em><NavLink to='/login'>Login</NavLink></em>
                    }
            </div>

        </header>
    </>
}

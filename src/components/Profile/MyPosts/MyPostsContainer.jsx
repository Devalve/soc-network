import React from 'react'
import { addPost } from '../../../redux/profileReducer'
import { MyPosts } from "./MyPosts"
import { connect } from 'react-redux'



class MyPostsContainer extends React.Component {
    render() {
        return <MyPosts
            posts={this.props.posts}
            addPost={this.props.addPost}
        />
    }
}

let mapStateToProps = state => {

    return {
        posts: state.profilePage.posts,
        newInputText: state.profilePage.newInputText,
    }
}
let mapDispatchToProps = dispatch => {
    return {
        addPost: newPostText => {
            dispatch(addPost(newPostText))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(MyPostsContainer)


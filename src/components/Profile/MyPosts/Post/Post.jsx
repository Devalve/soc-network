import React from 'react'
import s from './Post.module.scss'

export let Post = props => {

    return <div>

        <div className={s.item}>  <img src='https://yt3.ggpht.com/a/AATXAJy--4xEzAsjBdvrGj-DeqOxk_XyatMl3QaKAg=s900-c-k-c0xffffffff-no-rj-mo' alt="" />
            {props.msg}
            <div className={s.likeCount}>
                <span >like {props.likeCount}</span>
            </div>
        </div>

    </div>

}

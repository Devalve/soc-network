import React from 'react'
import { Post } from './Post/Post'
import { reduxForm, Field } from 'redux-form'
import s from './MyPosts.module.scss'
import '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { maxLengthIs, required } from '../../../utils/Validators/validators'
import { ElementType } from '../../../hoc/FormControl/FormConrtol'
const Textarea = ElementType('textarea')
const maxLength10 = maxLengthIs(10)
const AddPostForm = props => {


    return <div className={s.myPosts}>
        <form onSubmit={props.handleSubmit}>
            <div className={s.text}>
                <Field
                    name='newPostText'
                    className='form-control'
                    component={Textarea}
                    placeholder='Enter your post'
                    validate={[required,maxLength10]}
                />
            </div>
            <div className={s.wrapButton}>
                <button className='btn btn-primary' >
                    Add post
            </button>
            </div>

        </form>
    </div>

}

const MyPostsReduxForm = reduxForm({ form: 'MY_POSTS_FORM' })(AddPostForm)

export const MyPosts = props => {
    let postElement = props.posts.map(p => <Post msg={p.msg} likeCount={p.likeCount} />)
    let addPost = (values) => {
        props.addPost(values.newPostText)
    }
    return <>
        <div className={s.post}>
            {postElement}
        </div>
        <MyPostsReduxForm onSubmit={addPost} />
    </>
}
import React from 'react'
import './ProfileInfo.module.scss'
import s from './ProfileInfo.module.scss'
import { Preloader } from '../../Preloader/Preloader'
import { Status } from './Status/Status'


export let ProfileInfo = props => {
    let profile = props.profile
    if (!profile) {
        return <Preloader />
    }
    return <div  >

        <div >
            <figure>
                <img src={profile.photos.large} alt='pict' />
                <figcaption>{profile.fullName}</figcaption>
                <figcaption>
                    <Status
                        updateProfileStatus={props.updateProfileStatus}
                        status={props.status} />
                </figcaption>
            </figure>
        </div  >

        <div className={s.description}>
            <em> {profile.aboutMe}</em>
        </div>
        <div className={s.contacts}>
            Contacts:
            <div>
                {props.profile.contacts.github}
                <div>
                    {profile.contacts.vk}
                </div>
                <div>
                    {profile.contacts.instagram}
                </div>
                <div>
                    {profile.contacts.website}
                </div>
            </div>
        </div>
    </div>
}

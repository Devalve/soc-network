import React from 'react'
import s from './Status.module.scss'

export class Status extends React.Component {
    state = {
        editMode: false
    }

    activateEditMode = () => {

        this.setState({
            editMode: true,
            status: this.props.status
        })
    }
    deactivateEditMode = () => {
        this.setState({
            editMode: false
        })
        this.props.updateProfileStatus(this.state.status)
    }
    onChangeProfileStatus = (e) => {
        this.setState({
            status: e.currentTarget.value
        })
    }
    componentDidUpdate = (prevProps, prevState) => {

        if (prevProps.status !== this.props.status) {
            this.setState({
                status: this.props.status
            })
        }

    }

    render() {
        return <>
            {!this.state.editMode
                ? <span onDoubleClick={this.activateEditMode} >
                    {this.props.status || 'no status'}
                </span>
                : <input
                    autoFocus={true}
                    onBlur={this.deactivateEditMode}
                    value={this.state.status}
                    onChange={this.onChangeProfileStatus}
                />
            }
        </>
    }
}

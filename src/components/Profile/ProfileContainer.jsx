import React from 'react'
import { Profile } from './Profile'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { getProfile, getProfileStatus, updateProfileStatus } from '../../redux/profileReducer'
import { withRedirectComponent } from '../../hoc/Redirect'
import { compose } from 'redux'

class ProfileContainer extends React.Component {

    componentDidMount() {

        let userId = this.props.match.params.userId
        if (!userId) {
            userId = this.props.loggedUserId
        }
        this.props.getProfile(userId)
        this.props.getProfileStatus(userId)
    }


    render() {
        return <Profile {...this.props}
            profile={this.props.profile}
            updateProfileStatus={this.props.updateProfileStatus}
            status={this.props.status}
        />
    }



}

let mapStateToProps = state => ({
    profile: state.profilePage.profile,
    isFetching: state.usersPage.isFetching,
    status: state.profilePage.status,
    loggedUserId: state.auth.id,
    isAuth: state.auth.isAuth    
})


export default compose(connect(mapStateToProps, { getProfile, getProfileStatus, updateProfileStatus }),
    withRouter, withRedirectComponent)
    (ProfileContainer)



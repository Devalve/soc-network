import React from 'react'
import s from './DialogItem.module.scss'
import { NavLink } from 'react-router-dom'

export let DialogItem = (props) => {

    let path = "/dialogs/" + props.id;
    return <div>
        <div className={s.dialog} >
            <NavLink to={path} activeClassName={s.active}>{props.name}</NavLink>
        </div>
    </div>
}




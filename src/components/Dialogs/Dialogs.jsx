import React from 'react'
import s from './Dialogs.module.scss'
import { DialogItem } from './DialogItem/DialogItem'
import { Message } from './Messsage/Message'
import { reduxForm, Field } from 'redux-form'
import { maxLengthIs, required } from '../../utils/Validators/validators'
import { ElementType } from '../../hoc/FormControl/FormConrtol'

const TextArea = ElementType('textarea')
const maxLength50 = maxLengthIs(50)

const DialogsForm = props => {
    return <form onSubmit={props.handleSubmit}>
        <Field
            name='newMessageText'
            component={TextArea}
            className='form-control'
            placeholder='Enter your message'
            validate={[required, maxLength50]}
        />
        <div>
            <button className='btn btn-dark' >
                Send
                        </button>
        </div>
    </form>

}




const ReduxDialogsForm = reduxForm({ form: 'DIALOGS_FORM' })(DialogsForm)

export const Dialogs = props => {

    let dialogItem = props.dialogItem.map(d => <DialogItem name={d.name} id={d.id} key={d.id} />)
    let messageItem = props.messages.map(m => <Message msg={m.message} id={m.id} key={m.id} />)

    let addMessage = (values) => {
        props.addMessage(values.newMessageText)
    }

    return <>
        <div className={s.dialogs}   >

            <div className={s.dialog_item}>
                {dialogItem}
            </div >

            <div className={s.messages}>
                {messageItem}
            </div>

        </div>
        <ReduxDialogsForm onSubmit={addMessage} />
    </>
}



import React from 'react'
import s from './Message.module.scss'

export let Message = (props) => {
    return <div className={s.message}>{props.msg}</div>
}


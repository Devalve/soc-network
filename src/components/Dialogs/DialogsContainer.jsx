import { addMessage } from '../../redux/dialogReducer'
import React from 'react'
import { Dialogs } from './Dialogs'
import { connect } from 'react-redux'
import { withRedirectComponent } from '../../hoc/Redirect'
import { compose } from 'redux'


export const mapStateToProps = state => {
    return {
        messages: state.dialogPage.messages,
        dialogItem: state.dialogPage.dialogItem
    }
}
export const mapDispatchToProps = dispatch => {
    return {
        addMessage: newMessageText => {
            dispatch(addMessage(newMessageText))
        }
    }
}

export const DialogsContainer = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withRedirectComponent)
    (Dialogs)
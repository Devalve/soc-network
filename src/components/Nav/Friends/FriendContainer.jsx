import { connect } from 'react-redux'
import Friend from './Friend'

const mapStateToProps = state => {
    return {
        name: state.nav.name,
        src: state.nav.src
    }
}
const mapDispatchToProps = () => {
    return {}
}

export const FriendContainer = connect(mapStateToProps, mapDispatchToProps)(Friend)
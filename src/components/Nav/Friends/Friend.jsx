import React from 'react'
import s from './Friend.module.scss'
import { NavLink } from 'react-router-dom'



export let Friend = props => {
    return <NavLink to="#" activeClassName={s.active} >
        <figure>
            <img src={props.src} alt='pict' />
            <figcaption>{props.name}</figcaption>
        </figure>
    </NavLink>



}

import React from 'react'
import s from './Nav.module.scss'
import { NavLink } from 'react-router-dom'
import { Friend } from './Friends/Friend'

export let Nav = props => {

    let friendElement = props.friends.map(f => <Friend name={f.name} src={f.src} key={f.id} />)
    return <nav className={s.nav}>
        <div className={s.navItem}>
            <div className={s.item}>
                <NavLink to="/profile" activeClassName={s.active}>Profile</NavLink>
            </div>
            <div className={s.item}>
                <NavLink to="/dialogs" activeClassName={s.active}>Messages</NavLink>
            </div>
            <div className={s.item}>
                <NavLink to="/news" activeClassName={s.active}>News</NavLink>

            </div>
            <div className={s.item}>
                <NavLink to="/music" activeClassName={s.active}>Music</NavLink>
            </div>
            <div className={s.item}>
                <NavLink to="/settings" activeClassName={s.active}>Settings</NavLink>
            </div>
            <div className={s.item}>
                <NavLink to="/users" activeClassName={s.active}>Users</NavLink>
            </div>
            <div className={s.friends}>
                <p className={s.friendsTitle}>FRIENDS</p>
                {friendElement}
            </div>


        </div>

    </nav>

}

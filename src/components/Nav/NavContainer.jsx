import { connect } from 'react-redux'
import { Nav } from './Nav'


const mapStateToProps = state => {
    return {
        friends: state.nav.friends
    }
}

const mapDispatchToProps = dispatch => {
    return {}
}

export const NavContainer = connect(mapStateToProps, mapDispatchToProps)(Nav)

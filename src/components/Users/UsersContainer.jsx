import React from 'react'
import { Users } from './Users'
import { connect } from 'react-redux'
import {

    setTotalUsersCount,
    toggleFollow, getUsers, getUsersOnPageChange,
    follow, unfollow
} from '../../redux/usersReducer'

import { Preloader } from '../Preloader/Preloader'

class UsersContainer extends React.Component {

    componentDidMount() {
        this.props.getUsers(this.props.currentPage, this.props.pageSize)

    }

    onPageChanged = pageNum => {
        this.props.getUsersOnPageChange(pageNum, this.props.pageSize)
    }

    render() {

        return <>
            {this.props.isFetching ? <Preloader /> : null}
            <Users
                users={this.props.users}
                totalUsersCount={this.props.totalUsersCount}
                pageSize={this.props.pageSize}
                onPageChanged={this.onPageChanged}
                followInProgress={this.props.followInProgress}
                follow={this.props.follow}
                unfollow={this.props.unfollow}
            />
        </>

    }
}

const mapStateToProps = state => {
    return {
        users: state.usersPage.users,
        pageSize: state.usersPage.pageSize,
        totalUsersCount: state.usersPage.totalUsersCount,
        currentPage: state.usersPage.currentPage,
        isFetching: state.usersPage.isFetching,
        followInProgress: state.usersPage.followInProgress
    }

}




export const UserContainer = connect(mapStateToProps, { setTotalUsersCount, toggleFollow, getUsers, getUsersOnPageChange, follow, unfollow })(UsersContainer)
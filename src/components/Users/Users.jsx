import React from 'react'
import { User } from './User/User'
import s from './Users.module.scss'

export const Users = props => {
    let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize)
    let pages = []
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i)
    }
    let userElem = props.users.map(u =>
        <User
            users={props.users}
            id={u.id}
            name={u.name}
            status={u.status}
            photos={u.photos}
            follow={props.follow}
            unfollow={props.unfollow}
            followInProgress={props.followInProgress}
            followed={u.followed}
        />)




    return <>
        <div className={s.pagesCountContainer}>
            {pages.map(p => {
                return <span
                    onClick={() => { props.onPageChanged(p) }}
                    className={props.currentPage === p ? s.selected : s.unselected} >
                    {p}
                </span>
            })}
        </div>
        {userElem}
    </>
}
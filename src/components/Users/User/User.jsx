import React from 'react'
import s from './User.module.scss'
import photo from '../../../assets/img/users/defaultAvatar.png'
import { NavLink } from 'react-router-dom'


export let User = props => {

    return <div>
        <span>
            <div>
                <NavLink to={'/profile/' + props.id}>
                    <img className={s.userAvatar} alt=''
                        src={!props.photos.small
                            ? photo
                            : props.photos.small} />
                </NavLink>

            </div>
            <div>

                {props.followed

                    ? <button disabled={props.followInProgress.some(id => id === props.id)} onClick={() => {
                        props.unfollow(props.id)
                    }}>Unfollow</button>
                    : <button disabled={props.followInProgress.some(id => id === props.id)} onClick={() => {
                        props.follow(props.id)
                    }}>Follow</button>
                }

            </div>
        </span>
        <span>
            <span>
                <div>{props.name}</div>
                <div>{props.status}</div>
            </span>
            <span>
            </span>
        </span>
    </div >


}
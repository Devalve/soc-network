const ADD_MESSAGE = 'ADD-MESSAGE'

let initState = {
    messages: [
        { message: 'Welcome to Passione', id: 1 },
        { message: 'Yo', id: 2 },
        { message: 'Not a 4, pleeaaase!!!', id: 3 },
        { message: 'Yo', id: 4 }],
    dialogItem: [
        { name: 'Polpo', id: 1 },
        { name: 'Jiorno', id: 2 },
        { name: 'Mista', id: 3 },
        { name: 'Doppio', id: 4 }]
}

export const dialogReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_MESSAGE: {
            return {
                ...state,
                messages: [...state.messages, { message: action.newMessageText }]
            }
        }
        default: return state
    }
}
export const addMessage = newMessageText => ({ type: ADD_MESSAGE, newMessageText })
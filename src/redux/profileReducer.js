import { profileAPI } from "../api/api"
import { toggleFetch } from "./usersReducer"

const ADD_POST = 'ADD-POST'
const SET_USER_PROFILE = 'SET-USER-PROFILE'
const SET_PROFILE_STATUS = 'SET-PROFILE-STATUS'

let initState = {
    posts: [],
    newInputText: '',
    profile: null,
    status: ''
}

export const profileReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_POST: {
            let newPost = {
                msg: action.newPostText,
                likeCount: 0,
                id: state.posts.id += 1
            }
            return {
                ...state,
                newInputText: '',
                posts: [...state.posts, newPost]
            }
        }
        case SET_USER_PROFILE: {
            return {
                ...state,
                profile: action.profile
            }
        }
        case SET_PROFILE_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }

        default: return state
    }
}
export const addPost = newPostText => ({ type: ADD_POST, newPostText })
export const setProfile = profile => ({ type: SET_USER_PROFILE, profile })
export const setProfileStatus = status => ({ type: SET_PROFILE_STATUS, status })

export const getProfile = userId => dispatch => {

    profileAPI.getProfile(userId)
        .then(response => {
            dispatch(setProfile(response.data))
            dispatch(toggleFetch(false))
        })
}
export const getProfileStatus = userId => dispatch => {
    profileAPI.getStatus(userId)
        .then(response => {
            dispatch(setProfileStatus(response.data))
        })
}
export const updateProfileStatus = status => dispatch => {
    dispatch(toggleFetch(true))
    profileAPI.updateStatus(status)
        .then(response => {
            if (response.data.resultCode === 0) {
                dispatch(setProfileStatus(status))

            }
        })
}



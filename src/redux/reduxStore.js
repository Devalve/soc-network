import { createStore, combineReducers, applyMiddleware } from "redux"
import { profileReducer } from './profileReducer'
import { dialogReducer } from './dialogReducer'
import { navbarReducer } from './navbarReducer'
import { usersReducer } from "./usersReducer"
import { authReducer } from "./authReducer"
import { reducer as formReducer } from 'redux-form'
import { appReducer } from "./appReducer"
import thunkMiddleWare from 'redux-thunk'

let reducers = combineReducers({
    profilePage: profileReducer,
    dialogPage: dialogReducer,
    usersPage: usersReducer,
    nav: navbarReducer,
    auth: authReducer,
    form: formReducer,
    app: appReducer
})
export let store = createStore(reducers, applyMiddleware(thunkMiddleWare))
window.store = store



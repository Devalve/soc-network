import { usersAPI } from "../api/api"

const ADD_USER = 'ADD-USER'
const FOLLOW = 'FOLLOW'
const UNFOLLOW = 'UNFOLLOW'
const SET_CURRENT_PAGE = 'SET-CURRENT-PAGE'
const SET_TOTAL_USERS_COUNT = 'SET-TOTAL-USERS-COUNT'
const TOGGLE_IS_FETCHING = 'TOGGLE-IS-FETCHING'
const TOGGLE_FOLLOW = 'TOGGLE-FOLLOW'

let initState = {
    users: [],
    totalUsersCount: 0,
    pageSize: 5,
    currentPage: 1,
    isFetching: false,
    followInProgress: []
}

export const usersReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_USER: {
            return { ...state, users: action.users }

        }

        case FOLLOW: {
            return {
                ...state,
                users: state.users.map(u => {
                    if (u.id === action.userId) {
                        return { ...u, followed: true }
                    }
                    return u
                })
            }
        }
        case UNFOLLOW: {
            return {
                ...state,
                users: state.users.map(u => {
                    if (u.id === action.userId) {
                        return { ...u, followed: false }
                    }
                    return u
                })
            }
        }
        case SET_CURRENT_PAGE: {
            return { ...state, currentPage: action.currentPage }

        }
        case SET_TOTAL_USERS_COUNT: {
            return { ...state, totalUsersCount: action.count }

        }
        case TOGGLE_IS_FETCHING: {
            return { ...state, isFetching: action.isFetching }

        }
        case TOGGLE_FOLLOW: {
            return {
                ...state,
                followInProgress: action.isFetching
                    ? [...state.followInProgress, action.id]
                    : state.followInProgress.filter(id => id !== action.id)
            }

        }

        default: return state
    }
}


export const addUsers = users => ({ type: ADD_USER, users })
export const followSucc = userId => ({ type: FOLLOW, userId })
export const unfollowSucc = userId => ({ type: UNFOLLOW, userId })
export const setCurrentPage = currentPage => ({ type: SET_CURRENT_PAGE, currentPage })
export const setTotalUsersCount = count => ({ type: SET_TOTAL_USERS_COUNT, count })
export const toggleFetch = isFetching => ({ type: TOGGLE_IS_FETCHING, isFetching })
export const toggleFollow = (isFetching, id) => ({ type: TOGGLE_FOLLOW, isFetching, id })

export const getUsers = (currentPage, pageSize) => {
    return dispatch => {
        dispatch(toggleFetch(true))
        usersAPI.getUsers(currentPage, pageSize).then(data => {
            dispatch(toggleFetch(false))
            dispatch(addUsers(data.items))
            dispatch(setTotalUsersCount(data.totalCount))
        })
    }
}


export const getUsersOnPageChange = (currentPage, pageSize) => {
    return dispatch => {
        dispatch(toggleFetch(true))
        dispatch(setCurrentPage(currentPage))
        usersAPI.getUsers(currentPage, pageSize).then(data => {
            dispatch(toggleFetch(false))
            dispatch(addUsers(data.items))
            dispatch(setTotalUsersCount(data.totalCount))
        })
    }
}

export const follow = id => {
    return dispatch => {

        dispatch(toggleFollow(true, id))
        usersAPI.follow(id)
            .then(response => {
                if (response.data.resultCode === 0) {
                    dispatch(followSucc(id))
                }
                dispatch(toggleFollow(false, id))
            })
    }
}
export const unfollow = id => {
    return dispatch => {

        dispatch(toggleFollow(true, id))
        usersAPI.unfollow(id).then(response => {
            if (response.data.resultCode === 0) {
                dispatch(unfollowSucc(id))
            }
            dispatch(toggleFollow(false, id))
        })
    }
}
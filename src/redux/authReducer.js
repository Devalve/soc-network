import { authAPI } from "../api/api"
import { stopSubmit } from "redux-form"

const ADD_USERS_DATA = 'ADD-USERS-DATA'

let initState = {
    id: null,
    login: null,
    email: null,
    isAuth: false
}

export const authReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_USERS_DATA: {
            return {
                ...state,
                ...action.data
            }
        }
        default:
            return state
    }
}

export const addUserDataSucc = (id, login, email, isAuth) => ({ type: ADD_USERS_DATA, data: { id, login, email, isAuth } })


export const addUsersAuthData = () => dispatch => {
    return authAPI.me()
        .then(response => {
            if (response.data.resultCode === 0) {

                let { id, login, email } = response.data.data
                dispatch(addUserDataSucc(id, login, email, true))

            }
        })
}

export const logIn = (email, password, rememberMe) => dispatch => {

    authAPI.logIn(email, password, rememberMe)
        .then(response => {
            if (response.data.resultCode === 0) {
                dispatch(addUsersAuthData())
            } else {
                let message = response.data.messages[0]
                dispatch(stopSubmit('LOGIN', { _error: message }))
            }
        })
}
export const logOut = () => dispatch => {
    authAPI.logOut().then(response => {
        if (response.data.resultCode === 0) {
            dispatch(addUserDataSucc(null, null, null, false))
        }
    })
}

import { addUsersAuthData } from "./authReducer"

const INITIALIZE = 'INITIALIZE'

let initState = {
    initialized: false
}

export const appReducer = (state = initState, action) => {
    switch (action.type) {
        case INITIALIZE: {
            return {
                ...state,
                initialized: true
            }
        }
        default: return state
    }
}

export const initializeSuccess = () => ({ type: INITIALIZE })

export const initializeApp = () => dispatch => {
    let promise = dispatch(addUsersAuthData())
    Promise.all([promise])
        .then(() => {
            dispatch(initializeSuccess())
        })
}

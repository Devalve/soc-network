import Axios from 'axios'

let instance = Axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        "API-KEY": "71498abc-8b8c-4e45-a832-5c0715c416da"
    }
})

export const usersAPI = {
    getUsers(pageNum, pageSize) {
        return instance
            .get(`users?page=${pageNum}&count=${pageSize}`)
            .then(response => {
                return response.data
            })
    },
    follow(id) {

        return instance.post(`follow/${id}`)
    },
    unfollow(id) {

        return instance.delete(`follow/${id}`)
    }
}

export const authAPI = {
    me() {
        return instance.get(`auth/me`)
    },
    logIn(email, password, rememberMe = false) {
        return instance.post(`auth/login`, { email, password, rememberMe })
    },
    logOut() {
        return instance.delete(`auth/login`)
    }
}

export const profileAPI = {
    getProfile(id) {
        return instance.get(`profile/` + id)
    },
    getStatus(id) {
        return instance.get(`profile/status/` + id)
    },
    updateStatus(status) {
        return instance.put(`profile/status`, { status })
    }
}
